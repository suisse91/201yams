#!/usr/bin/env ruby
# coding: utf-8

require("./proba.rb")

class Paire < Proba
  def initialize dices, nb
    @Dices = dices
    @Nb = nb
    @Diff = 0
    @Throw = 0
    @Dices.each { |i| @Throw += 1 if i != @Nb}
    (5 - @Throw) > 2 ? @Diff = 0 : @Diff = 2 - (5 - @Throw)
    if @Nb < 1 || @Nb > 6
      puts "Error: number must be between 1 and 6"
      exit
    end
  end

  def same n
    cnp(@Throw, n) * (1.0/6)**n * (5.0/6)**(@Throw - n)
  end

  def chances
    res = 0
    (@Diff..@Throw).each {|i| res += same(i)}
    res *= 100
    puts "Probabilité d'obtenir un double de #{@Nb}: #{res.round(2)}%"
  end
end
