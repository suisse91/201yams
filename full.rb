#!/usr/bin/env ruby
# coding: utf-8

require("./proba.rb")

class Full < Proba
  def initialize dices, vals
    @Dices = dices
    @Brelan = vals[0].to_i
    @Double = vals[1].to_i
    countDicesToThrow
  end

  def countDicesToThrow
    cval1 = 0
    cval2 = 0
    @Dices.each { |i|
      cval1 += 1 if i == @Brelan
      cval2 += 1 if i == @Double
    }
    cval1 > 2 ? @Wanted1 = 0 : @Wanted1 = 3 - cval1
    cval2 > 1 ? @Wanted2 = 0 : @Wanted2 = 2 - cval2
  end

  def chances
    res = cnp(@Wanted1 + @Wanted2, @Wanted1) * 1.0 / 6**(@Wanted1 + @Wanted2)
    res *= 100
    puts "Probabilité d'avoir un full de #{@Brelan} par les #{@Double}: #{res.round(2)}%"
  end
end
