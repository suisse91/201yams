#!/usr/bin/env ruby
# coding: utf-8

require("./proba.rb")

class Suite < Proba
  def initialize dices, nb
    @Dices = dices.sort
    @Nb = nb
    @NbDices = 0
    @Diff = Array.new(7, 0)
    if (![5,6].include? @Nb)
      puts "Error: number must be 5 or 6"
      exit
    end
    @Dices.each { |i| @Diff[i] += 1 }
    @Diff.each_with_index { |val, idx|
      if idx == 1 and @Nb == 6
        @NbDices += val
      elsif idx == 6 and @Nb == 5
        @NbDices += val
      else
        @NbDices += (val -1) if val > 1
      end
    }
  end

  def chances
    res = fact(@NbDices) / 6.0**@NbDices * 100
    puts "Probabilité d'obtenir une grande suite : #{res.round(2)}%"
  end
end
