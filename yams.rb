#!/usr/bin/env ruby
# coding: utf-8

require './proba.rb'

class Yams < Proba
  def initialize dices, p
    @Dices = dices
    @Nb = 0
    @Wanted = p
    dices.each {|x| @Nb += 1 unless x == p}
  end

  def chances
    res = (1.0/6)**@Nb
    res *= 100
    puts "Probabilité d'obtenir un yams de #{@Wanted}: #{res.round(2)}%"
  end
end
