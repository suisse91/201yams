#!/usr/bin/env ruby
# coding: utf-8

require('./yams.rb')
require('./full.rb')
require('./paire.rb')
require('./suite.rb')
require('./carre.rb')
require('./brelan.rb')

def checkType
  ['paire', 'brelan', 'carre', 'yams', 'suite', 'full'].include? ARGV[5][/[a-z]*/]
end

def checkCmd
  dices = Array.new
  5.times { |i| dices << ARGV[i].to_i}
  calculator = 0
  if !checkType
    puts "Error: type is unknown"
    return
  end
  if ARGV[5].include? "full"
    calculator = Full.new(dices, ARGV[5][/[0-9]_[0-9]/].split('_'))
  else
    calculator = Object.const_get(ARGV[5][/[a-z]*/].capitalize).new(dices, ARGV[5][/[0-9]/].to_i)
  end
  calculator.chances
end

def main
  if ARGV.length != 6
    puts "Usage: ./201yams [Dices values] [Type]"
    return
  end
  checkCmd
end

main
