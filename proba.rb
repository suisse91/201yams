#!/usr/bin/env ruby

class Proba
  def initialize dices
    @Dices = dices
  end

  private
  
  def fact n
    (n == 0) ? 1 : (n * fact(n - 1))
  end

  def cnp n, p
    exit if (p > n)
    res = fact(n) / (fact(p) * fact(n - p))
  end
end
